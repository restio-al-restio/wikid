# usage: sh -x autogen.sh

for p in guile guile-baux-tool gnulib-tool autoconf automake ; do
    $p --version | sed 's/^/using: /;1q'
done

guile-baux-tool snuggle m4 build-aux

autoreconf -I build-aux --install --force --verbose -Wall

actually ()
{
    gnulib-tool --copy-file $1 $2
}
actually build-aux/install-sh
actually doc/INSTALL.UTF-8 INSTALL

# autogen.sh ends here
