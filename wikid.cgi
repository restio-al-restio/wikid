;;; wikid.cgi						-*- scheme -*-

;; Copyright (C) 2001, 2002, 2004, 2007, 2012, 2021 Thien-Thi Nguyen
;;
;; This file is part of WIKID, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

(exit #f) ;;; cannot be run w/o config (do not modify this line!)

(use-modules
 ((srfi srfi-13) #:select (substring/shared))
 ((ice-9 rdelim) #:select (read-line)))

(cond-expand
 (guile-2 (use-modules
           (ice-9 iconv)
           (rnrs bytevectors)))
 (else #f))

(define msg (let ((raw (case (string->symbol (getenv "REQUEST_METHOD"))
                         ((POST) (read-line))
                         ((GET)  (getenv "QUERY_STRING"))
                         (else ""))))
              (if (string-null? raw)
                  "index"
                  raw)))

(define len (string-length msg))

(define sock
  (catch #t
         (lambda ()
           (let ((sock (socket (#:#sock-type-sel
                                PF_UNIX
                                PF_INET)
                               SOCK_STREAM 0)))
             (#:#sock-type-sel
              (connect sock AF_UNIX CFG:listener-port)
              (connect sock AF_INET INADDR_LOOPBACK CFG:listener-port))
             sock))
         list))

(define SEND
  (cond-expand
   (guile-2 (lambda (sock string)
              (send sock (string->bytevector string "UTF-8"))))
   (else send)))

(or (and (file-port? sock)
         (not (port-closed? sock)))
    (let ((CRLF (string #\cr #\newline)))

      (define (fso s . args)
        (apply simple-format #t s args))

      (define (fso-h k v)
        (fso "~A: ~A~A" k v CRLF))

      (fso-h 'Connection 'close)
      (fso-h 'Content-type 'text/plain)
      (display CRLF)
      (fso "badness! -- cannot contact wikid on port ~A~%"
           CFG:listener-port)
      (fso "discarding message (~A bytes):~%~A~%" len msg)
      (fso "~%~%(sorry!  try again later.)~%")
      (let ((type (car sock))
            (args (cdr sock)))

        (define (display-error-message)
          (and=> (car args)
                 (lambda (proc)
                   (fso "in procedure ‘~A’:~%\t" proc)))
          (apply fso (cadr args) (caddr args)))

        (define (pebkac blurb)
          (fso "~%(~A, please file a bug report: <mailto:~A>)~%"
               blurb BUGREPORT))

        (fso "btw, problem was:~%\t")
        (case type
          ((system-error misc-error)
           (display-error-message))
          ((wrong-type-arg unbound-variable)
           (display-error-message)
           (pebkac "programmer error"))
          (else
           (write sock)
           (pebkac "unrecognized problem type"))))
      (exit #t)))

(SEND sock
      ;; This is a poor man's ‘(format "~8,'0X" len)’.
      ;; An alternate, perhaps easier to understand, algorithm:
      ;;
      ;;  (let ((s (number->string len 16)))
      ;;    (string-append (make-string (- 8 (string-length s))
      ;;                                #\0)
      ;;                   s))
      ;;
      ;; The current one avoids ‘let’ and is more parallelizable,
      ;; given a "sufficiently smart compiler".
      (string-append
       (make-string (- 7 (ash (integer-length (ash len -1))
                              -2))
                    #\0)
       (number->string len 16)))
(SEND sock msg)

(define MKBUF
  (cond-expand (guile-2 make-bytevector)
               (else make-string)))

(define SQUEEZE
  (cond-expand (guile-2 (lambda (end buf)
                          (let ((b (make-bytevector end)))
                            (bytevector-copy! buf 0 b 0 end)
                            (bytevector->string b "UTF-8"))))
               (else (lambda (end buf)
                       (substring/shared buf 0 end)))))

(define return-message
  (let ((buf (MKBUF CFG:max-msg-size)))
    (let loop ((i 0))

      ;; This is a kludge for some versions of guile that use a static
      ;; `scm_addr_buffer' (libguile/socket.c) that is consulted by
      ;; `recvfrom!'.  If it is empty, the address family is incorrectly
      ;; interpreted as AF_UNSPEC.  The `getsockname' fills this buffer.
      (getsockname sock)

      (let ((got (car (recvfrom! sock buf 0 i))))
        (if (or (not got)
                (zero? got))
            (SQUEEZE i buf)
            (loop (+ i got)))))))

(display return-message)

(exit #t)

;;; wikid.cgi ends here
