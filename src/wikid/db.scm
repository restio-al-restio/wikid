;;; db.scm

;; Copyright (C) 2001, 2002, 2004, 2007, 2012, 2021 Thien-Thi Nguyen
;;
;; This file is part of WIKID, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

(define-module (db)
  #:export (object-filename
            wikid-db-connect)
  #:use-module ((database gdbm) #:select (gdbm-fetch
                                          gdbm-store!
                                          gdbm-exists?
                                          gdbm-first-key
                                          gdbm-next-key
                                          gdbm-open
                                          gdbm-delete!)))

(define object-filename (make-object-property))

(define (wikid-db-connect var-dir open-mode)

  (define (make-table stem)
    (let ((filename (simple-format #f "~A~A.db" var-dir stem))
          (gdbm #f))

      (define (get key)
        (gdbm-fetch gdbm key))

      (define (put! key val)
        (gdbm-store! gdbm key val 'replace))

      (define (exists? key)
        (gdbm-exists? gdbm key))

      (define (all-keys)
        (let loop ((key (gdbm-first-key gdbm)) (acc '()))
          (if key
              (loop (gdbm-next-key gdbm key)
                    (cons key acc))
              acc)))

      (define (all)
        (let loop ((key (gdbm-first-key gdbm)) (acc '()))
          (if key
              (loop (gdbm-next-key gdbm key)
                    (acons key (gdbm-fetch gdbm key)
                           acc))
              acc)))

      (define (init)
        (set! gdbm (gdbm-open filename open-mode))
        (set! (object-filename gdbm) filename)
        gdbm)

      (define (del! key)
        (gdbm-delete! gdbm key))

      (lambda (choice)
        (case choice
          ((#:get) get)
          ((#:put!) put!)
          ((#:exists?) exists?)
          ((#:all-keys) all-keys)
          ((#:all) all)
          ((#:init) init)
          ((#:del!) del!)
          ((#:gdbm) gdbm)
          (else (error "bad choice:" choice))))))

  (let ((i-body  (make-table 'i-body))
        (title-i (make-table 'title-i))
        (extra   (make-table 'extra)))

    (define (max-i)
      (apply max -1 (map string->number ((i-body #:all-keys)))))

    (define (new-body! title body)
      (let* ((known? ((title-i #:exists?) title))
             (i (if known?
                    ((title-i #:get) title)
                    (number->string (1+ (max-i))))))
        (or known?
            ((title-i #:put!) title i))
        ((i-body #:put!) i body)
        (list 'id i)))

    (define (promote! title url)
      (and=> ((title-i #:get) title)
             (lambda (i)
               ((title-i #:del!) title)
               ((i-body  #:del!) i)))
      ((extra #:put!) title url))

    (for-each (lambda (table)
                ((table #:init)))
              (list i-body title-i extra))
    (lambda (command . args)
      (case command
        ((#:known?) ((title-i #:exists?) (car args)))
        ((#:body) ((i-body #:get) ((title-i #:get) (car args))))
        ((#:new-body!) (apply new-body! args))
        ((#:all-titles) ((title-i #:all)))
        ((#:ext-get) ((extra #:get) (car args)))
        ((#:ext-put) (apply promote! args))
        ((#:ext-all) ((extra #:all)))
        ((#:gdbm) (map (lambda (table) (table #:gdbm))
                       (list i-body title-i extra)))
        (else (error "bad command:" command))))))

;; db.scm ends here
