;;; render.scm

;; Copyright (C) 2001, 2002, 2004, 2007, 2012, 2021 Thien-Thi Nguyen
;;
;; This file is part of WIKID, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; "wref" is "wiki-ref", basically text between "<<" and ">>".

;;; Code:

(define-module (render)
  #:export (make-renderer)
  #:use-module ((www url-coding) #:select (url-coding:encode))
  #:use-module ((base) #:select (s<-tree))
  #:use-module ((ttn-do zzz xhtml-tree) #:select (~input
                                                  ~textarea
                                                  ~a ~html
                                                  ~head ~meta
                                                  ~title ~body
                                                  ~h1 ~h2 ~ul ~li ~ul
                                                  ~b ~p ~form ~hr))
  #:use-module ((ttn-do mogrify) #:select (editing-buffer))
  #:use-module ((srfi srfi-13) #:select (string-count)))

;; support

(define (stars->lists buf pmin pmax)
  (editing-buffer buf
    (goto-char pmin)
    (and (re-search-forward "^( *)\\*" pmax #t)
         (let ((prefix-re (apply string-append
                                 `("^"
                                   ,@(make-list (- (match-end 1)
                                                   (match-beginning 1))
                                                "[ ]")
                                   "\\*"))))
           (replace-match "<ul><li>")
           (while (re-search-forward prefix-re pmax #t)
             ;; Better to use looking-at here, but there's weird corruption
             ;; going on that not even match-data and set-match-data can fix.
             (cond ((string=? "-" (buffer-substring (point) (1+ (point))))
                    (delete-char 1) (delete-char -1)
                    (insert "</ul>")
                    (break))
                   (else
                    (replace-match "<li>"))))
           ;; children
           (stars->lists buf pmin (point))
           ;; sibling tail call
           (stars->lists buf (point) pmax)))))

(define (simple-message title . body)
  (~html (~head (~title title))
         (~body body)))

(define (titled title . body)
  (simple-message title (~h1 title) body))

(define (uenc title)
  (url-coding:encode title #f))

;; closure

(define (make-renderer dispatch DB)

  (define (disp . rest) (cons dispatch rest))

  (define (disp-p title)
    (disp "?p=" (uenc title)))

  (define (wlink title)
    (~a 'href (disp-p title)
        'title title
        title))

  (define (disp-e title) (disp "?e=" (uenc title)))
  (define (disp-a title) (disp "?a=" (uenc title)))

  (define (html<-wikid-text body)
    (editing-buffer body
      (define (change old new)
        (goto-char (point-min))
        (let ((replacement (if (thunk? new)
                               new
                               (lambda ()
                                 new))))
          (while (re-search-forward old (point-max) #t)
            (replace-match (replacement)))))
      (change "<<([^>]+)>>"
              (lambda ()
                (s<-tree (let ((wref (match-string 1)))
                           (cond ((DB #:ext-get wref)
                                  => (lambda (val)
                                       (~a 'href val wref)))
                                 (else
                                  (wlink wref)))))))
      (stars->lists (current-buffer) (point-min) (point-max))
      (change "\r\n" "\n")
      (change "\n\n" "\n<p>\n")
      (buffer-string)))

  (define (render-all-buttons title)
    (list (~a 'href (disp-e title) "(edit this page)")
          (~a 'href (disp-a title) "(append to this page)")
          (~a 'href (disp "?index") "(index)")))

  (define (form blurb title preamble hidden body rows go)
    (titled (list blurb ": " title)
            preamble
            " When you are done, select \"" go "\" below."
            (~p)
            (render-all-buttons title)
            (~form 'method "POST"
                   'action dispatch
                   (~hr)
                   (~input 'type "HIDDEN"
                           'name (car hidden)
                           'value (cdr hidden))
                   (if rows
                       (~textarea 'rows rows
                                  'cols 72
                                  'name (car body)
                                  (cdr body))
                       (~input 'type "text"
                               'size 72
                               'name (car body)
                               'value (cdr body)))
                   (~hr)
                   (~input 'type "SUBMIT"
                           'value go))))

  (define (index)
    (define (group blurb ls proc)
      (list (~h2 blurb)
            (~ul (map (lambda (pair)
                        (~li (proc (car pair) (cdr pair))))
                      (sort ls (lambda (a b)
                                 (string<? (car a) (car b))))))))
    (titled "index"
            (group "pages" (DB #:all-titles)
                   (lambda (title i)
                     (list (~a 'href (disp-e title) "(edit)")
                           " "
                           (wlink title))))
            (group "extra links" (DB #:ext-all)
                   (lambda (key val)
                     (~a 'href val key)))))

  (define (page title)
    (let ((body (html<-wikid-text (DB #:body title))))
      (if (string-null? body)
          (form "no text" title
                (list "This page has no text.  You have two choices: "
                      (~ul
                       (~li (~a 'href (disp-e title) "edit it to add text"))
                       (~li "change the reference <<"
                            (~b title) ">> to point to a URL.")))
                (cons "promote" title)
                (cons "url" "http://")
                #f "change")
          (titled title
                  (~hr)
                  body
                  (~hr)
                  (render-all-buttons title)
                  (strftime "<br>(rendered %Y-%m-%d %T UTC)"
                            (gmtime (current-time)))))))

  (define (edit-form title)
    (let ((body (DB #:body title)))
      (form "editing" title
            (list "You can change the contents of \"" title "\" here"
                  " by editing the text in the form.")
            (cons "change" title)
            (cons "body" body)
            (+ 10 (string-count body #\newline))
            "send")))

  (define (append-form title)
    (form "appending to" title
          (list "You can append content to \"" title "\" here"
                " by writing new text in the form.")
          (cons "append" title)
          (cons "comment" "")
          15 "send"))

  (define (update-ok title new-pages)
    (let ((pretty (list "update ok: " title))
          (present-url (disp-p title)))
      (~html
       (~head (~meta 'HTTP-EQUIV "Refresh"
                     'Content (list "20"
                                    "; URL="
                                    present-url))
              (~title pretty))
       (~body (~h1 pretty)
              "Looks like the update to \"" (~b title) "\" went well! "
              "You can wait for the redirect (20sec), or click "
              (~a 'href present-url "here") "."
              (~hr)
              "New pages:"
              (if (null? new-pages)
                  " (none)"
                  (~ul (map (lambda (title)
                              (~li (wlink title)))
                            new-pages)))
              (~hr)
              (render-all-buttons title)))))

  ;; rv
  (lambda (sel)
    (case sel
      ((#:simple-message) simple-message)
      ((#:index)          index)
      ((#:page)           page)
      ((#:edit-form)      edit-form)
      ((#:append-form)    append-form)
      ((#:update-ok)      update-ok)
      (else (error "bad sel:" sel)))))

;;; render.scm ends here
