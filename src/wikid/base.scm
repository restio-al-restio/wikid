;;; base.scm

;; Copyright (C) 2012, 2021 Thien-Thi Nguyen
;;
;; This file is part of WIKID, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

(define-module (base)
  #:export (s<-tree)
  #:use-module ((ttn-do zzz publishing) #:select (flatten-to)))

(define (s<-tree tree)
  (flatten-to #f tree))

;;; base.scm ends here
