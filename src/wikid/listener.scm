;;; listener.scm

;; Copyright (C) 2001, 2002, 2004, 2007, 2012, 2021 Thien-Thi Nguyen
;;
;; This file is part of WIKID, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

(define-module (listener)
  #:export (make-listener)
  #:use-module ((www url-coding) #:select (url-coding:decode))
  #:use-module ((srfi srfi-2) #:select (and-let*))
  #:use-module ((srfi srfi-13) #:select ((substring/shared . shsub)
                                         string-index))
  #:use-module ((srfi srfi-19) #:select ((current-time . this-moment)
                                         time-difference
                                         time-second
                                         time-nanosecond)))

(cond-expand
 (guile-2 (use-modules
           (ice-9 iconv)
           (rnrs bytevectors)))
 (else #f))

(define SEND
  (cond-expand
   (guile-2 (lambda (port string)
              (send port (string->bytevector string "UTF-8"))))
   (else send)))

(define RECV!
  (cond-expand
   (guile-2 (lambda (port s)
              (let* ((len (string-length s))
                     (b (make-bytevector len 0))
                     (rv (recv! port b)))
                (set! b (bytevector->string b "UTF-8"))
                (do ((i 0 (1+ i)))
                    ((= len i))
                  (string-set! s i (string-ref b i)))
                rv)))
   (else recv!)))

(define (make-listener fL cfg handle-command)
  (let ((buf (make-string (cfg #:max-msg-size))))

    (define sock!
      (let ((port (cfg #:listener-port)))

        (define (streamed family)
          (socket family SOCK_STREAM 0))

        (define (UNIX command)
          (let ((filename port))
            (case command
              ((prepare!) (let ((sock (streamed PF_UNIX)))
                            (bind sock AF_UNIX filename)
                            sock))
              ((cleanup!) (delete-file filename))
              (else #t))))

        (define (INET command)
          (case command
            ((prepare!) (let ((sock (streamed PF_INET)))
                          (setsockopt sock SOL_SOCKET SO_REUSEADDR 1)
                          (bind sock AF_INET INADDR_LOOPBACK port)
                          sock))
            ((cleanup!) #t)
            (else (let ((conn command))
                    (= INADDR_LOOPBACK (sockaddr:addr conn))))))

        ;; sock!
        (if (string? port)
            UNIX
            INET)))

    (define (handle port conn)

      (define (out! string)
        (SEND port string))

      (define (decode)

        (define (sub beg end)
          (shsub buf beg end))

        (define (next char beg end)
          (string-index buf char beg end))

        (define (one beg end)
          (cond ((next #\= beg end)
                 => (lambda (idx)
                      (cons
                       (string->symbol (sub beg idx))
                       (url-coding:decode (sub (1+ idx) end)))))
                (else
                 (list (string->symbol (sub beg end))))))

        (define (truly)
          (and-let* ((len-buf (make-string 8))
                     ((= 8 (RECV! port len-buf)))
                     (len (string->number len-buf 16))
                     (sz (RECV! port buf))
                     ((= len sz)))
            (let loop ((start 0) (acc '()))
              (cond ((next #\& start len)
                     => (lambda (amp)
                          (loop (1+ amp) (cons (one start amp) acc))))
                    (else
                     (reverse! (cons (one start len)
                                     acc)))))))

        (cond ((not (sock! conn))
               "sorry, not authorized\n")
              ((truly)
               => (lambda (cmd)
                    (fL "~A" (if (list? cmd) (car cmd) cmd))
                    cmd))
              (else
               "some kind of error, hmm")))

      (define (respond!)
        (out! (handle-command (decode))))

      (define (timed thunk)
        (let* ((start-time (this-moment))
               (ignored-rv (thunk))
               (diff (time-difference (this-moment) start-time)))
          (+ (* 1e+3 (time-second diff))
             (* 1e-6 (time-nanosecond diff)))))

      (fL "~A msec" (timed respond!)))

    ;; rv
    (lambda ()
      (fL "Restart")
      (let ((sock (sock! 'prepare!))
            (signalled? #f))

        (define (bye sig)
          (fL "got signal: ~A" sig)
          ;; If we were signalled before but are still stuck in
          ;; ‘accept’, then go ahead and bail.
          (and signalled? (throw 'exit-now))
          (set! signalled? #t))

        (for-each (lambda (signal)
                    (sigaction signal bye))
                  (list SIGHUP SIGINT SIGQUIT SIGALRM))
        (listen sock 10)
        (catch #t
               (lambda ()
                 (let loop ()
                   (let* ((pair (accept sock))
                          (port (car pair))
                          (conn (cdr pair)))
                     (dynamic-wind
                         (lambda () #f)
                         (lambda () (handle port conn))
                         (lambda () (fL "") (shutdown port 2))))
                   (or signalled? (loop))))
               (lambda (key . args)
                 (or (eq? 'exit-now key)
                     (fL "listener caught `~A' ~S" key args))))
        (close-port sock)
        (sock! 'cleanup!)))))

;;; listener.scm ends here
