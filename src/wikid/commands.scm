;;; commands.scm

;; Copyright (C) 2001, 2002, 2004, 2007, 2012, 2021 Thien-Thi Nguyen
;;
;; This file is part of WIKID, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

(define-module (commands)
  #:export (make-command-handler)
  #:use-module ((base) #:select (s<-tree))
  #:use-module ((ttn-do mogrify) #:select (editing-buffer)))

(define (make-command-handler fL R DB)
  (let ((simple-message (R #:simple-message))
        (edit-form (R #:edit-form))
        (append-form (R #:append-form))
        (known-page (R #:page)))

    (define (ensure-existence title)
      ;; Return ‘#t’ if a new page was created.
      (and (not (DB #:known? title))
           (begin (fL "created page: ~A ~A"
                      title
                      (DB #:new-body! title ""))
                  #t)))

    (define (create-new-pages body)
      ;; Scan BODY for references w/o pages and create them.
      ;; Return a list of TITLEs.
      (let ((new-pages '()))
        (editing-buffer body
          (goto-char (point-min))
          (while (re-search-forward "<<([^>]+)>>" (point-max) #t)
            (let ((wref (match-string 1)))
              (or (DB #:ext-get wref)
                  (and (ensure-existence wref)
                       (set! new-pages (cons wref new-pages)))))))
        (reverse! new-pages)))

    (define (page title)
      (ensure-existence title)
      (known-page title))

    (define (note-change blurb title body)
      (DB #:new-body! title body)
      (fL "~A: ~A -- now ~A bytes"
          blurb title (string-length body))
      ((R #:update-ok) title (create-new-pages body)))

    (define (change-body title body)
      (note-change "updated" title body))

    (define (append-comment title comment)
      (note-change "appended to" title
                   (string-append (DB #:body title)
                                  "\n\n<HR>\n\n"
                                  comment)))

    (define (promote-url title url)
      (DB #:ext-put title url)
      (fL "promoted: ~A (~A)" title url)
      (simple-message "great!" "ok (no worries)"))

    (define (normal alist)
      (let ((command-table
             `(((p)              . ,page)
               ((e)              . ,edit-form)
               ((a)              . ,append-form)
               ((index)          . ,(lambda (x) ((R #:index))))
               ((change body)    . ,change-body)
               ((append comment) . ,append-comment)
               ((promote url)    . ,promote-url))))
        ;; avoid `assoc': alist not guaranteed to be ordered
        (or (or-map (lambda (command)
                      (let ((keys (car command))
                            (proc (cdr command)))
                        (and (= (length keys) (length alist))
                             (and-map (lambda (key)
                                        (assq key alist))
                                      keys)
                             (or (apply proc (map (lambda (key)
                                                    (assq-ref alist key))
                                                  keys))
                                 #t))))
                    command-table)
            (simple-message
             "badness!" "something bad happened, sorry"))))

    (define (as mime-type x)
      (let ((CRLF "\r\n"))
        (list "Connection: close" CRLF
              "Content-Type: " mime-type CRLF
              CRLF
              x)))

    ;; rv
    (lambda (x)
      (s<-tree
       (if (pair? x)
           (as "text/html" (normal x))
           (as "text/plain" x))))))

;;; commands.scm ends here
