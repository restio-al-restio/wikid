;;; config.scm

;; Copyright (C) 2001, 2002, 2004, 2007, 2012, 2021 Thien-Thi Nguyen
;;
;; This file is part of WIKID, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; (read-wikid-config port validate?) => REPORT
;;
;; REPORT is a proc that returns a value given key PART,
;; which can be one of (with type):
;;
;;      name            -- symbol
;;      dispatch        -- string
;;      fs-cgi          -- string
;;      listener-port   -- filename (fifo) or number (TCP port)
;;      max-msg-size    -- number
;;      var-dir         -- string
;;      log-file        -- string or #f
;;      pid-file        -- string

;;; Code:

(define-module (config)
  #:export (read-wikid-config))

(define (read-wikid-config filename validate?)

  (define (file-ok? x)
    (and (string? x)
         (file-exists? x)))

  (define (dir-ok? x)
    (and (file-ok? x)
         (file-is-directory? x)))

  (define (parent-dir-ok? x)
    (dir-ok? (dirname x)))

  (define (badness blurb x)
    (throw 'bad-config (string-append blurb ":") x))

  (let ((ok `((dispatch #:dispatch      ,string?)
              (fs-cgi   #:fs-cgi        ,file-ok?)
              (port     #:listener-port ,(lambda (x)
                                           (or (string? x)
                                               (number? x))))
              (max-msg  #:max-msg-size  ,number?)
              (var-dir  #:var-dir       ,dir-ok?)
              (log-file #:log-file      ,parent-dir-ok?)
              (pid-file #:pid-file      ,parent-dir-ok?)))
        (form (call-with-input-file filename read)))
    (or (symbol? (car form)) (badness "not a symbol" (car form)))
    (let loop ((fields (cdr form)))
      (or (null? fields)
          (let ((one (car fields)))
            (or (pair? one) (badness "not a pair" one))
            (cond ((and (symbol? (car one))
                        (assq (car one) ok))
                   => (lambda (ent)
                        (set-car! one (cadr ent))
                        (and validate?
                             (or ((caddr ent) (cdr one))
                                 (badness "invalid value" (cdr one))))))
                  (else (badness "invalid field" (car one))))
            (loop (cdr fields)))))
    (let ((full (acons 'name (car form)
                       (cdr form))))
      (for-each (lambda (internal)
                  (or (assq-ref full internal)
                      (badness "missing field" internal)))
                (map cadr ok))
      ;; return the report
      (lambda (part)
        (or (assq-ref full part)
            (error "invalid part:" part))))))

;;; config.scm ends here
