\input texinfo   @c -*-texinfo-*-
@c %**start of header
@setfilename wikid.info
@documentencoding UTF-8
@settitle WIKID
@paragraphindent 0
@setchapternewpage odd
@c %**end of header

@include version.texi

@c Copyright (C) 2002, 2004, 2007, 2012, 2021 Thien-Thi Nguyen
@c
@c This file is part of WIKID, released under the terms of the
@c GNU General Public License as published by the Free Software
@c Foundation; either version 3, or (at your option) any later
@c version.  There is NO WARRANTY.  See file COPYING for details.

@c directory meta info
@direntry
* WIKID: (wikid).               Wiki Daemon.
@end direntry

@ifinfo
This file documents the WIKID program.

Copyright 2002, 2004, 2007, 2012, 2021 Thien-Thi Nguyen

Permission is granted to make and distribute verbatim copies of
this manual provided the copyright notice and this permission notice
are preserved on all copies.
@end ifinfo

@c  This title page illustrates only one of the
@c  two methods of forming a title page.

@titlepage
@title WIKID Manual
@author Thien-Thi Nguyen

@c  The following two commands
@c  start the copyright page.
@page
@vskip 0pt plus 1filll
Copyright @copyright{} 2002, 2004, 2007, 2012, 2021 Thien-Thi Nguyen

Published by Thien-Thi Nguyen

Permission is granted to make and distribute verbatim copies of
this manual provided the copyright notice and this permission notice
are preserved on all copies.
@end titlepage

@contents

@c ---------------------------------------------------------------------------
@ifnottex
@node Top
@top WIKID

This document describes how to setup and use WIKID @value{VERSION}, a
package to operate a "wiki" instance in cooperation with a web server
(such as Apache).
@end ifnottex

@menu
* Copying::             Your rights and freedoms.
* Admin::               Administering a wiki instance.
* User::                Using the wiki instance.
* Hacker::              Improving WIKID.

* Index::
@end menu

@c ---------------------------------------------------------------------------
@node    Copying
@chapter Copying
@cindex  Copying

WIKID is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

WIKID is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Emacs; see the file COPYING.  If not, write to the
Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
Boston, MA 02110-1301, USA.

@c ---------------------------------------------------------------------------
@node    Admin
@chapter Admin
@cindex  administration

Like all web services, WIKID requires some preparation to get a proper
@dfn{wiki instance} up and running.  These steps are separate from those
required for package installation (i.e., "configure && make && make install");
they are taken after such installation.

To make administration both easier and more reproducible, each wiki instance
relies on a configuration file (@pxref{config file format}).  To create one
from template, use the @code{wikid-admin create-config} command:

@example
wikid-admin create-config /etc/my-wiki.conf
@end example

This creates @file{/etc/my-wiki.conf}, a plain text file that you should now
edit to taste.  All other administrative tasks (as performed by
@code{wikid-admin}) require this file to be specified on the command line,
using @code{--config} (or the short form @code{-C}):

@example
wikid-admin --config /etc/my-wiki.conf [COMMAND [ARGS ...]]
@end example

In the following descriptions, for brevity we use @dfn{$WA} to mean
@code{wikid-admin --config CONFIG-FILE}.

@menu
* config file format::                  Defining a WIKID instance.
* wikid-admin init::                    Setting up a WIKID instance.
* daemon control::                      Starting / stopping / getting status.
* wikid-admin dump-db::                 Looking at the content.
* wikid-admin dump-db-as-seed::         Same, but now machine-readable.
* where to start::                      Static/dynamic entry.
@end menu

@c ---------------------------------------------------------------------------
@node    config file format
@section config file format
@cindex  config file format
@cindex  dispatch
@cindex  fs-cgi
@cindex  port
@cindex  max-msg
@cindex  var-dir
@cindex  log-file
@cindex  pid-file

Here are the notes from the example wikid.conf distributed with WIKID, and
also installed in $prefix/share/wikid (by default -- check your installation
for the actual path).

@example
This file defines the configuration for one instance of WIKID.
Canonical form:

(NAME
 ;; This is a symbol (no quotes), unlike the strings below.

 (dispatch . "/LOGICAL/PATH/TO/wikid.cgi")
             ;; This is the path passed back into the web server so be
             ;; aware of "ScriptAlias" (if using Apache) and the like.

 (fs-cgi   . "/ABSOLUTE/PATH/TO/wikid.cgi")
             ;; This is the filesystem path where the wikid.cgi is to
             ;; be installed (at which time other config values are
             ;; hard-coded into the executable for speed).  Note that
             ;; the basename should match the one used for `dispatch'.

 (port     . NUMBER-OR-STRING)
             ;; If a number, it is a port for PF_INET INADDR_LOOPBACK
             ;; (ie, localhost) listening.  If a string, it is a
             ;; filename for PF_UNIX listening, in which case the
             ;; daemon creates and removes the "port" socket file
             ;; as necessary.

 (max-msg  . NUMBER-OF-BYTES)
             ;; This limits the size of the messages sent between
             ;; wikid.cgi and the daemon.  User-visible web pages
             ;; can never exceed NUMBER-OF-BYTES (and will probably
             ;; be less due to HTTP and other overhead).

 (var-dir  . "/DIRECTORY/FOR/RUNTIME-INFO/")
             ;; The page databases are kept in this directory.
             ;; It should end in a slash ("/").

 (log-file . "/PATH/TO/LOGFILE")
             ;; This can also be set to the value #f (without quotes)
             ;; for no log file.

 (pid-file . "/PATH/TO/PIDFILE")
             ;; This file contains the process id (PID) of the daemon
             ;; during operation.  When the daemon is shut down using
             ;; "wikid-admin -C config stop", this file is deleted.

 )
 ;; end of canonical form

Note the "." between the field names and the field values.
@end example

You can use the convenience command @code{wikid-admin create-config FILE}
to copy the example wikid.conf to @file{FILE} where you can edit it.  After
editing, @xref{wikid-admin init}.

@c ---------------------------------------------------------------------------
@node    wikid-admin init
@section wikid-admin init
@cindex  initializing a wiki instance

Usage: $WA init [DATA-FILE-1 DATA-FILE-2 ...] [LINKS-FILE-1 ...]

The @dfn{init} command creates backend database files in the @dfn{var-dir}
specified in the configuration file (@pxref{config file format}) from the
@dfn{seed data} given files.  Also, directories for the log-file and pid-file
are created (if necessary), and a CGI script is installed in the location
specified by @dfn{fs-cgi} and made executable.

The two kinds of seed inputs, data files, and links files, can be specified in
any order.

Each DATA-FILE has format described here: the first line is "Title:" followed
by the title, terminating with newline.  The rest of the file is the body.  A
very simple page is shown here:

@example
Title: some topic of interest, hopefully
Topics are interest are interesting by definition,
which in itself is not so interesting.  So it goes.
@end example

Each LINKS-FILE begins with "Extra:" on a line by itself.  Subsequent lines
should be composed of a text description, followed by a TAB character,
followed by a URL, and finished with newline.  Here is an example:

@example
Extra:
GNU	http://www.gnu.org/
WIKID homepage	http://people/ttn/software/wikid/
@end example

If no files are specified, @code{wikid-admin init} uses some example pages
and extra links from the WIKID distribution.

@c ---------------------------------------------------------------------------
@node    daemon control
@section daemon control
@cindex  daemon control

@cindex starting the daemon
@cindex daemon, start
Usage: $WA start

Start the daemon.  The daemon writes its process ID to the specified
@code{pid-file} and begins to listen on the specified @code{port} for
commands.  It appends progress messages to the specified @code{log-file}.

NOTE: Make sure you set up the config file (@pxref{Admin}) and do the proper
initializations first (@pxref{wikid-admin init}).

@cindex stopping the daemon
@cindex daemon, stop
Usage: $WA stop

Stop the daemon, deleting the @code{pid-file} as well.

@cindex getting status from the daemon
@cindex daemon, status
Usage: $WA status

Display the pid daemon if it is running, otherwise "not running".

@c ---------------------------------------------------------------------------
@node    wikid-admin dump-db
@section wikid-admin dump-db
@cindex  database, dumping
@cindex  database, browsing

Usage: $WA dump-db

The pairs in each database file are printed to standard output in an
unspecified order.  The file names are also displayed.  This command doesn't
understand the semantic role of any such database file in implementing the
relations wikid needs at runtime, so the output is non-specialized.

@xref{wikid-admin dump-db-as-seed}.

@c ---------------------------------------------------------------------------
@node    wikid-admin dump-db-as-seed
@section wikid-admin dump-db-as-seed
@cindex  database, dumping as seed dir
@cindex  database, moving

Usage: $WA dump-db-as-seed DIR

The @dfn{dump-db-as-seed} command writes out the database files in a format
that can be read back by @xref{wikid-admin init}.  DIR is created if it does
not exist.  This is useful for moving or archiving a WIKID instance that has
been going for a while.  For example, to move:

@example
$ wikid-admin -C /etc/wikid.conf stop
$ sed 's,/www/var,/www/var2,' /etc/wikid.conf > /etc/wikid2.conf
$ wikid-admin -C /etc/wikid.conf dump-db-as-seed /tmp/wikid2-seed
$ wikid-admin -C /etc/wikid2.conf init /tmp/wikid2-seed/*
$ rm -rf /tmp/wikid2-seed
@end example

@c ---------------------------------------------------------------------------
@node    where to start
@section where to start

The two commands @code{index} and @code{display-start-url}
provide static and dynamic, respectively, methods for you to
``enter'' the wiki.

@subsection index
Usage: $WA index

Display to standard out an HTML page titled "index" with a list of direct
links to each page in the database, as well as links to a page for editing
those pages.  Note that if you save this page it will be outdated if new pages
are added to the wiki.

@subsection display-start-url
Usage: $WA display-start-url [host]

Display to standard out the URL of the page where the index is dynamically
generated by the daemon.  This is useful in conjunction with the shell's
command substitution facilities.  For example (using bash syntax):

@example
$ w3m $($WA display-start-url)
@end example

Optional arg @var{host} specifies another host (you can @code{:PORT} if
desired) to use instead of the default @code{localhost}.  If your browser
supports CGI filename remapping (like w3m), you can specify @var{host} to be
@code{-} (a single hyphen) to omit the @code{http://HOST} part entirely.

@c ---------------------------------------------------------------------------
@node    User
@chapter User

@cindex using a wiki instance
@cindex editing
Using a wiki instance run by WIKID is pretty easy: just read the web pages,
and follow the various links both internal and external.  The bottom of each
page shows when the page was rendered (not exactly the same as when it was
written --- that will be included in a future release).

This chapter explains what to do when you want to go beyond this passive usage
mode and edit the pages.

@cindex markup items
When you select @dfn{edit this page}, the text given to you to edit (also
known as the @dfn{page source}) may have a few @dfn{markup items} that are
interpreted specially when rendering the page to HTML.  At this time, there
are three types of markup item: the wref, the star-list and the blank line.

@cindex wref
@cindex wiki reference
The @dfn{wref} or @dfn{wiki reference} is text enclosed in double
angle-brackets that specify the title of another page in this wiki instance,
which are rendered as hyperlinks to the specified pages.  For example:

@example
<<first wiki page>>   becomes   <A HREF="...">first wiki page</A>
<<foo bar baz>>       becomes   <A HREF="...">foo bar baz</A>
@end example

@cindex list-star
The @dfn{star-list} is a some set of text whose left-most margin is delimited
by optional spaces and a @code{*} (star).  The special item @dfn{*-} (star
hyphen) terminates the list.  Lists are rendered using @code{<UL>} containers
and can nest.  For example:

@example
* first           becomes       <UL><LI> first
* second                        <LI> second
  * sub-first                   <UL><LI> sub-first
  * sub-second                  <LI> sub-second
  *-                            </UL>
* third<BR>                     <LI> third<BR>
  with extra filler               with extra filler
*-                              </UL>
@end example

Note that there can be intervening text ("with extra filler") between the list
items, as long as that text does not begin with @code{*} (star).

@cindex blank line
The only other markup item recognized by WIKID rendering is a blank line,
which is replaced by the @code{<P>} paragraph tag.  Any other HTML is passed
straight through.  [This will probably become more restrictive in the future
to prevent abuse.]

@cindex committing the edit
After you are finished editing the page source, select the @samp{send} link to
commit the changes.  WIKID shows an "ok" message if things are ok, and your
browser should refresh to the updated page (you may need to reload).

@cindex new pages
@section New Pages
Upon committing the edit, the "ok" page lists any new pages that may have been
created (simply by using an unused wref).  Selecting the associated link shows
a dialogue where you can choose to either add text to the new (empty) page, or
@dfn{promote} the wref to a URL.  In the latter case, type in the URL and and
select @samp{change}.

That's it!  Have fun!

@c ---------------------------------------------------------------------------
@node    Hacker
@chapter Hacker

WIKID can be improved in many ways.  (This is another way of saying it is
very far from perfect!)  Here are some ideas, by no means exhaustive, to get
you started:

@itemize

@item Replace listener w/ GNU Serveez.  This would also have the benefit
of wikid being able to use fifos (and whatever else Serveez supports in
the future).

@item Reorgnanize database tables.  Currently there are four tables, two of
which are used for reverse mapping, somewhat of a waste.  It would be better
to use one table for the pages and have the data be in rfc822-compliant form.
This allows easy representation of Title, URL, Last-Update, Permissions, and
other meta-data, which can be kept in core and crossreferenced as needed.

@item Make page header, footer, etc (styles / eye-candy) configurable.

@item Use correct HTTP caching protocols.  At the moment, WIKID simply uses
the "Refresh" for the "ok" page, but doesn't add anything for other pages.

@item Make markup items configurable.

@item Internationalize buttons, etc.

@item Use cookies (gasp, horror) to save user preferences (if any).

@item Add "wikid-admin archive [DIR]" command.  The archive directory should be
specifiable in the configuration file.

@item Make "wikid-admin init" handle single arg DIR instead of requiring
explicit listing of all files (interpret DIR as "DIR/*" and do globbing).

@item Come up w/ a way to have special pages trigger other cgi scripts.  For
example, a wikid page used for @dfn{code analysis} could trigger the running
of @samp{guile-tools lint}, which would be appended.

@item Augment "append" functionality to mimic bulletin boards, etc.

@item Keep statistics, publish them.

@item Enable easy access to published archives (if any).  This is the
so-called "right to fork" support.

@end itemize

Please feel free to discuss these or propose new ideas (as long as they don't
support for "WikiWords" :-) on the @samp{guile-user} mailing list.  You can
post patches to @samp{guile-sources} or send them to the author directly.

@c ---------------------------------------------------------------------------
@node Index
@unnumbered Index
@printindex cp

@c ---------------------------------------------------------------------------
@bye

@c wikid.texi ends here
