#!/bin/sh
exec ${GUILE-@GUILE@} -L '##-WIKID-DATADIR-##/modules' -s $0 "$@" # -*-scheme-*-
!#
;;; wikid-admin

;; Copyright (C) 2001, 2002, 2004, 2007, 2012, 2021 Thien-Thi Nguyen
;;
;; This file is part of WIKID, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: wikid-admin create-config FILE
;; Usage: wikid-admin --config FILE COMMAND [ARGS...]
;;
;; For the first usage, write a new configuration FILE from template.
;; All other invocations require that `--config FILE' be specified.
;; (You can use `-C' for `--config'.)  Commands are:
;;
;;  init [DATA-FILE-1 DATA-FILE-2 ...]
;;  dump-db
;;  dump-db-as-seed OUT-DIR
;;  index
;;  start
;;  stop
;;  status
;;  display-start-url [HOST]
;;
;; See WIKID info page node "Admin" for more info.

;;; Code:

(define-module (wikid-admin))

(define wikid-datadir (dirname (car %load-path)))

(use-modules ((ttn-do zzz banalities) #:select (check-hv
                                                qop<-args))
             ((ttn-do zzz filesystem) #:select (not-dot-not-dotdot
                                                filtered-files))
             ((ttn-do mogrify) #:select (editing-buffer))
             ((srfi srfi-13) #:select (string-trim-both))
             ((ttn-do zzz publishing) #:select (flatten))
             ((config) #:select (read-wikid-config))
             ((render) #:select (make-renderer))
             ((db) #:select (object-filename
                             wikid-db-connect))
             ((database gdbm) #:select (gdbm-store!
                                        gdbm-first-key
                                        gdbm-fetch
                                        gdbm-next-key)))

(define *config* #f)                    ; set by wikid-admin/qop

(define (fs s . args)
  (apply simple-format #f s args))

(define (fso s . args)
  (apply simple-format #t s args))

(define (mkdir-p dir)
  (or (and (file-exists? dir)
           (file-is-directory? dir))
      (begin
        (mkdir-p (dirname dir))
        (mkdir dir)
        (fso "Created directory ~A~%" dir)))
  dir)

(define (init-cgi!)
  (define (file-if-in dir)
    (let ((filename (in-vicinity dir "wikid.cgi")))
      (and (file-exists? filename) filename)))
  (let ((fs-cgi (*config* #:fs-cgi)))
    (editing-buffer (open-input-file
                     (cond ((file-if-in wikid-datadir))
                           ((file-if-in "."))
                           (else (error "could not find template wikid.cgi"))))
      (goto-char (point-min))
      (insert "#!@GUILE@ -s\n!#\n")
      (re-search-forward "^.exit #f.*cannot be run.*\n")
      (replace-match
       (fs "(define ~A ~S)~%(define ~A ~S)~%"
           "CFG:listener-port" (*config* #:listener-port)
           "CFG:max-msg-size"  (*config* #:max-msg-size)))
      (insert `(define BUGREPORT "@PACKAGE_BUGREPORT@")
              "\n")
      (let ((sel (if (string? (*config* #:listener-port))
                     car
                     cadr)))
        (while (search-forward "#:#sock-type-sel" #f #t)
          (replace-match "")
          (let* ((p (1- (match-beginning 0)))
                 (real (begin (goto-char p)
                              (sel (read (buffer-port))))))
            (delete-region p (point))
            (insert real))))
      (write-to-port (and (mkdir-p (dirname fs-cgi))
                          (open-output-file fs-cgi))))
    (fso "Wrote ~A~%" fs-cgi)
    (chmod fs-cgi #o755)))

(define (init-tables! file inc-reg! inc-ext! i-body title-i extra)
  (editing-buffer (open-input-file file)
    (goto-char (point-min))
    (cond ((looking-at "^Title:([^\n]+)\n")
           (inc-reg!)
           (goto-char (match-end 0))
           (let ((base (basename file))
                 (tsub (string-trim-both (match-string 1))))
             (for-each (lambda (table key value)
                         (gdbm-store! table key value 'insert))
                       (list i-body title-i)
                       (list base tsub)
                       (list (string-trim-both
                              (buffer-substring (point) (point-max)))
                             base))))
          ((looking-at "^Extra:\n")
           (while (re-search-forward "^(.+)\t(.+)$"
                                     (point-max) #t)
             (let ((key (match-string 1))
                   (val (match-string 2)))
               (inc-ext!)
               (gdbm-store! extra key val 'insert))))
          (else (error "bad input file:" file)))))

(define (init-db! files)
  (let* ((var-dir (mkdir-p (*config* #:var-dir)))
         (regular-count 0) (extra-count 0)
         (inc-reg! (lambda () (set! regular-count (1+ regular-count))))
         (inc-ext! (lambda () (set! extra-count (1+ extra-count))))
         (all-gdbm ((wikid-db-connect var-dir 'new) #:gdbm)))
    (for-each (lambda (file)
                (apply init-tables! file inc-reg! inc-ext! all-gdbm))
              files)
    (for-each (lambda (table)
                (fso "Wrote ~A~%" (object-filename table)))
              all-gdbm)
    (fso "Stats: ~A titled pages, ~A extra links~%"
         regular-count extra-count)))

(define *commands* '())                 ; alist

(define-macro (define-command name needs-config? needs-args . body)
  `(set! *commands* (acons ',name (vector ,needs-config?
                                          ,needs-args
                                          (lambda (args) ,@body))
                           *commands*)))

(define-command create-config #f 1
  (let ((new-config (car args)))
    (mkdir-p (dirname new-config))
    (copy-file (fs "~A/wikid.conf" wikid-datadir) new-config)
    (fso "Wrote new config file ~A~%" new-config)
    (fso "Now you must edit it (read the manual first!)~%")))

(define-command init #t #t
  (let ((dd (fs "~A/seed-data" wikid-datadir))
        (given args))
    (for-each (lambda (runtime-file)
                (mkdir-p (dirname (*config* runtime-file))))
              '(#:log-file #:pid-file))
    (init-db!
     (if (null? given)
         (let ((dir (cond ((file-exists? dd) dd)
                          ((file-exists? "./seed-data") "./seed-data")
                          (else (error "could not find seed-data dir")))))
           (fso "Using default seed-data from ~A~%" dir)
           (map (lambda (name) (in-vicinity dir name))
                (filtered-files not-dot-not-dotdot dir)))
         given))
    (init-cgi!)))

(define-command dump-db #t #f
  (for-each (lambda (table)
              (fso "~%(table ~S)~%" (object-filename table))
              (let loop ((key (gdbm-first-key table)))
                (cond (key
                       (fso "(k ~S~% v ~S)~%"
                            key (gdbm-fetch table key))
                       (loop (gdbm-next-key table key))))))
            ((wikid-db-connect (*config* #:var-dir) 'read) #:gdbm)))

(define-command dump-db-as-seed #t #t
  (let ((dir (mkdir-p (car args))))
    (define (dump i-body title-i extra)
      (let loop ((title (gdbm-first-key title-i)))
        (cond (title
               (let* ((i (gdbm-fetch title-i title))
                      (body (gdbm-fetch i-body i))
                      (file (in-vicinity dir i)))
                 (with-output-to-file file
                   (lambda ()
                     (fso "Title: ~A~%~%~A~%" title body)))
                 (fso "Wrote ~A -- ~A~%" file title))
               (loop (gdbm-next-key title-i title)))))
      (let ((extra-data-file (in-vicinity dir "extra"))
            (count 0))
        (with-output-to-file extra-data-file
          (lambda ()
            (fso "Extra:~%")
            (let loop ((key (gdbm-first-key extra)))
              (if key
                  (let ((val (gdbm-fetch extra key)))
                    (set! count (1+ count))
                    (for-each display (list key #\ht val #\nl))
                    (loop (gdbm-next-key extra key)))))))
        (fso "~A extras written to ~A~%" count extra-data-file)))
    (apply dump ((wikid-db-connect (*config* #:var-dir) 'read) #:gdbm))))

(define-command index #t #f
  (flatten (((make-renderer (*config* #:dispatch)
                            (wikid-db-connect (*config* #:var-dir) 'read))
             #:index))))

(define-command start #t #f
  (let ((pid-file (*config* #:pid-file)))
    (and (file-exists? pid-file)
         (error (fs "pid file exists: ~A" pid-file)))
    (system (fs "@GUILE@ -L ~A/modules -s ~A/wikid ~A &"
                wikid-datadir wikid-datadir
                (object-filename *config*)))
    (sleep 1)
    (fso "WIKID started --- pid ~A --- log-file ~A~%"
         (read (open-input-file pid-file))
         (*config* #:log-file))))

(define-command stop #t #f
  (let ((pid-file (*config* #:pid-file)))
    (or (file-exists? pid-file)
        (error (fs "pid file does not exist: ~A" pid-file)))
    (let ((pid (read (open-input-file pid-file))))
      (kill pid SIGHUP)
      (let loop ((count 0))
        (sleep 1)
        (cond ((= 5 count)
               (error "pid file still exists (5 attempts)"))
              ((file-exists? pid-file)
               (false-if-exception (kill pid SIGHUP))
               (loop (1+ count)))))
      (fso "WIKID stopped --- pid ~A~%" pid))))

(define-command status #t #f
  (let ((pid-file (*config* #:pid-file)))
    (fso "WIKID status: ~A~%"
         (if (file-exists? pid-file)
             (fs "running with pid ~A" (read (open-input-file pid-file)))
             "not running"))))

(define-command display-start-url #t #f
  (let* ((host (and (not (null? args))
                    (car args)))
         (no-scheme (and host (string=? "-" host))))
    (fso "~A~A~A~%"
         (if no-scheme "" "http://")
         (cond (no-scheme "")
               (host)
               (else "localhost"))
         (*config* #:dispatch))))

(define (wikid-admin/qop qop)
  (cond ((and (not (null? (qop '())))
              (string->symbol (car (qop '()))))
         => (lambda (cmd)
              (define (missing something)
                (error "missing" something))
              (let* ((v (or (assq-ref *commands* cmd)
                            (error "no such command")))
                     (needs-config? (vector-ref v 0))
                     (needs-args    (vector-ref v 1))
                     (proc          (vector-ref v 2))
                     (args          (cdr (qop '()))))
                (and needs-args (null? args)
                     (missing (if (and (number? needs-args)
                                       (= 1 needs-args))
                                  "arg"
                                  "args")))
                (and needs-config?
                     (or (qop 'config
                              (lambda (f)
                                (set! *config* (read-wikid-config f #f))
                                (set! (object-filename *config*) f)))
                         (missing "required flag: --config FILE")))
                (proc args))))
        (else
         (error "no command specified"))))

(define (main args)
  (check-hv args '((package . "@PACKAGE_NAME@")
                   (version . "@PACKAGE_VERSION@")
                   (help . commentary)))
  (exit (wikid-admin/qop
         (qop<-args args '((config (single-char #\C) (value #t)))))))

(main (command-line))

;;; wikid-admin ends here
